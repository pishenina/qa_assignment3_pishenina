import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select


def check_exists_by_xpath(xpath):
    try:
        driver.find_element(By.XPATH, xpath)
    except NoSuchElementException:
        print('Can`t create new account')
        return False
    return True


s = Service("C:\\Users\\Sasha\\Downloads\\chromedriver_win32\\chromedriver.exe")
driver = webdriver.Chrome(service=s)
driver.maximize_window()
driver.get("http://www.fb.com")
print(driver.current_url)
if driver.current_url == "https://www.facebook.com/":
    if check_exists_by_xpath("//*[text()='Создать новый аккаунт']"):
        driver.find_element(By.XPATH, "//*[text()='Создать новый аккаунт']")
        driver.implicitly_wait(10)
        driver.find_element(By.XPATH, "//*[text()='Создать новый аккаунт']").click()
        driver.find_element(By.NAME, "firstname").send_keys("Maria")
        driver.find_element(By.NAME, "lastname").send_keys("Jane")
        driver.find_element(By.NAME, "reg_email__").send_keys("lab3testmyname@gmail.com")
        driver.find_element(By.ID, "password_step_input").send_keys("12345pass")
        day = Select(driver.find_element(By.NAME, "birthday_day"))
        day.select_by_visible_text("8")
        month = Select(driver.find_element(By.NAME, "birthday_month"))
        month.select_by_visible_text("авг")
        year = Select(driver.find_element(By.NAME, "birthday_year"))
        year.select_by_visible_text("2000")
        driver.find_element(By.XPATH, "//label[text()='Женщина']").click()
        driver.find_element(By.NAME, "reg_email_confirmation__").send_keys("lab3testmyname@gmail.com")
        driver.find_element(By.XPATH, "//button[text()='Регистрация']").click()
else:
    print('Wrong url')
